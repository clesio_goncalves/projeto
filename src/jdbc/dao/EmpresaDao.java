package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.modelo.Empresa;

public class EmpresaDao {

	// a conexão com o banco de dados
	private Connection connection;

	public EmpresaDao(Connection connection) {
		this.connection = connection;
	}

	public void adiciona(Empresa empresa) {
		String sql = "insert into Empresa "
				+ "(cnpj_empresa,inscricao_estadual,nome_empresarial,nome_fantasia, grau_risco_empresa, cnae_empresa, atividade_principal, telefone_empresa, email_empresa, cep, nome_logradouro, numero_logradouro, bairro, uf, cidade, complemento)"
				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			// prepared statement para inserção
			PreparedStatement stmt = this.connection.prepareStatement(sql);

			// seta os valores
			stmt.setString(1, empresa.getCnpj_empresa());
			stmt.setString(2, empresa.getInscricao_estadual());
			stmt.setString(3, empresa.getNome_empresarial());
			stmt.setString(4, empresa.getNome_fantasia());
			stmt.setInt(5, empresa.getGrau_risco_empresa());
			stmt.setString(6, empresa.getCnae_empresa());
			stmt.setString(7, empresa.getAtividade_principal());
			stmt.setString(8, empresa.getTelefone_empresa());
			stmt.setString(9, empresa.getEmail_empresa());
			stmt.setString(10, empresa.getCep());
			stmt.setString(11, empresa.getNome_logradouro());
			stmt.setInt(12, empresa.getNumero_logradouro());
			stmt.setString(13, empresa.getBairro());
			stmt.setString(14, empresa.getUf());
			stmt.setString(15, empresa.getCidade());
			stmt.setString(16, empresa.getComplemento());

			// executa
			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	public List<Empresa> getLista() {
		try {
			List<Empresa> empresas = new ArrayList<Empresa>();
			PreparedStatement stmt = this.connection
					.prepareStatement("select * from Empresa");
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				// criando o objeto Empresa
				Empresa empresa = new Empresa();
				empresa.setCodigo_empresa(rs.getInt("codigo_empresa"));
				empresa.setCnpj_empresa(rs.getString("cnpj_empresa"));
				empresa.setInscricao_estadual(rs
						.getString("inscricao_estadual"));
				empresa.setNome_empresarial(rs.getString("nome_empresarial"));
				empresa.setNome_fantasia(rs.getString("nome_fantasia"));
				empresa.setGrau_risco_empresa(rs.getInt("grau_risco_empresa"));
				empresa.setCnae_empresa(rs.getString("cnae_empresa"));
				empresa.setAtividade_principal(rs
						.getString("atividade_principal"));
				empresa.setTelefone_empresa(rs.getString("telefone_empresa"));
				empresa.setEmail_empresa(rs.getString("email_empresa"));
				empresa.setCep(rs.getString("cep"));
				empresa.setNome_logradouro(rs.getString("nome_logradouro"));
				empresa.setNumero_logradouro(rs.getInt("numero_logradouro"));
				empresa.setBairro(rs.getString("bairro"));
				empresa.setUf(rs.getString("uf"));
				empresa.setCidade(rs.getString("cidade"));
				empresa.setComplemento(rs.getString("complemento"));

				// adicionando o objeto à lista
				empresas.add(empresa);

			}
			rs.close();
			stmt.close();
			return empresas;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public void editar(Empresa empresa) {
		String sql = "update Empresa set cnpj_empresa=?, inscricao_estadual=?, nome_empresarial=?, nome_fantasia=?, grau_risco_empresa=?, cnae_empresa=?, atividade_principal=?, telefone_empresa=?, email_empresa=?, cep=?, nome_logradouro=?, numero_logradouro=?, bairro=?, uf=?, cidade=?, complemento=?  where codigo_empresa=?";

		try {
			PreparedStatement stmt = this.connection.prepareStatement(sql);

			stmt.setString(1, empresa.getCnpj_empresa());
			stmt.setString(2, empresa.getInscricao_estadual());
			stmt.setString(3, empresa.getNome_empresarial());
			stmt.setString(4, empresa.getNome_fantasia());
			stmt.setInt(5, empresa.getGrau_risco_empresa());
			stmt.setString(6, empresa.getCnae_empresa());
			stmt.setString(7, empresa.getAtividade_principal());
			stmt.setString(8, empresa.getTelefone_empresa());
			stmt.setString(9, empresa.getEmail_empresa());
			stmt.setString(10, empresa.getCep());
			stmt.setString(11, empresa.getNome_logradouro());
			stmt.setInt(12, empresa.getNumero_logradouro());
			stmt.setString(13, empresa.getBairro());
			stmt.setString(14, empresa.getUf());
			stmt.setString(15, empresa.getCidade());
			stmt.setString(16, empresa.getComplemento());
			stmt.setInt(17, empresa.getCodigo_empresa());

			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public void excluir(Empresa empresa) {
		try {

			PreparedStatement stmt = this.connection
					.prepareStatement("delete from Empresa where codigo_empresa=?");
			stmt.setInt(1, empresa.getCodigo_empresa());

			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public List<Empresa> select(Empresa empresa) {
		try {
			List<Empresa> empresas = new ArrayList<Empresa>();
			PreparedStatement stmt = this.connection
					.prepareStatement("select * from Empresa where codigo_empresa=?");
			stmt.setInt(1, empresa.getCodigo_empresa());
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				// criando o objeto Empresa
				Empresa empresaResult = new Empresa();
				empresaResult.setCodigo_empresa(rs.getInt("codigo_empresa"));
				empresaResult.setCnpj_empresa(rs.getString("cnpj_empresa"));
				empresaResult.setInscricao_estadual(rs
						.getString("inscricao_estadual"));
				empresaResult.setNome_empresarial(rs
						.getString("nome_empresarial"));
				empresaResult.setNome_fantasia(rs.getString("nome_fantasia"));
				empresaResult.setGrau_risco_empresa(rs
						.getInt("grau_risco_empresa"));
				empresaResult.setCnae_empresa(rs.getString("cnae_empresa"));
				empresaResult.setAtividade_principal(rs
						.getString("atividade_principal"));
				empresaResult.setTelefone_empresa(rs
						.getString("telefone_empresa"));
				empresaResult.setEmail_empresa(rs.getString("email_empresa"));
				empresaResult.setCep(rs.getString("cep"));
				empresaResult.setNome_logradouro(rs
						.getString("nome_logradouro"));
				empresaResult.setNumero_logradouro(rs
						.getInt("numero_logradouro"));
				empresaResult.setBairro(rs.getString("bairro"));
				empresaResult.setUf(rs.getString("uf"));
				empresaResult.setCidade(rs.getString("cidade"));
				empresaResult.setComplemento(rs.getString("complemento"));

				// adicionando o objeto à lista
				empresas.add(empresaResult);

			}
			rs.close();
			stmt.close();
			return empresas;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
