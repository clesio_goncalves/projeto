package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.modelo.Usuario;

public class UsuarioDao {

	// a conexão com o banco de dados
	private Connection connection;

	public UsuarioDao(Connection connection) {
		this.connection = connection;
	}

	public void adiciona(Usuario usuario) {
		String sql = "insert into Usuario " + "(email_usuario,senha_usuario)"
				+ " values (?,?)";
		try {
			// prepared statement para inserção
			PreparedStatement stmt = this.connection.prepareStatement(sql);

			// seta os valores
			stmt.setString(1, usuario.getEmail_usuario());
			stmt.setString(2, usuario.getSenha_usuario());

			// executa
			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	public List<Usuario> getLista() {
		try {
			List<Usuario> usuarios = new ArrayList<Usuario>();
			PreparedStatement stmt = this.connection
					.prepareStatement("select * from Usuario");
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				// criando o objeto Usuario
				Usuario usuario = new Usuario();
				usuario.setCodigo_usuario(rs.getInt("codigo_usuario"));
				usuario.setEmail_usuario(rs.getString("email_usuario"));
				usuario.setSenha_usuario(rs.getString("senha_usuario"));

				// adicionando o objeto à lista
				usuarios.add(usuario);

			}
			rs.close();
			stmt.close();
			return usuarios;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public void editar(Usuario usuario) {
		String sql = "update Usuario set email_usuario=?, senha_usuario=? where codigo_usuario=?";

		try {
			PreparedStatement stmt = this.connection.prepareStatement(sql);

			stmt.setString(1, usuario.getEmail_usuario());
			stmt.setString(2, usuario.getSenha_usuario());
			stmt.setInt(3, usuario.getCodigo_usuario());

			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public void excluir(Usuario usuario) {
		try {

			PreparedStatement stmt = this.connection
					.prepareStatement("delete from Usuario where codigo_usuario=?");
			stmt.setInt(1, usuario.getCodigo_usuario());

			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public List<Usuario> select(Usuario usuario) {
		try {
			List<Usuario> usuarios = new ArrayList<Usuario>();
			PreparedStatement stmt = this.connection
					.prepareStatement("select * from Usuario where codigo_usuario=?");
			stmt.setInt(1, usuario.getCodigo_usuario());
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				// criando o objeto Usuario
				Usuario usuarioResult = new Usuario();
				usuarioResult.setCodigo_usuario(rs.getInt("codigo_usuario"));
				usuarioResult.setEmail_usuario(rs.getString("email_usuario"));
				usuarioResult.setSenha_usuario(rs.getString("senha_usuario"));

				// adicionando o objeto à lista
				usuarios.add(usuarioResult);

			}
			rs.close();
			stmt.close();
			return usuarios;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public int count(Usuario usuario) {
		try {
			PreparedStatement stmt = this.connection
					.prepareStatement("select count(*) from Usuario");
			ResultSet rs = stmt.executeQuery();

			int cont = 0;

			while (rs.next()) {
				cont = rs.getInt("count(*)");
			}
			rs.close();
			stmt.close();
			return cont;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public boolean login(Usuario usuario) {
		try {
			PreparedStatement stmt = this.connection
					.prepareStatement("SELECT email_usuario, senha_usuario FROM Usuario;");
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				// criando o objeto usuario
				String email_usuario = rs.getString("email_usuario");
				String senha_usuario = rs.getString("senha_usuario");

				if (usuario.getEmail_usuario().equals(email_usuario)
						&& usuario.getSenha_usuario().equals(senha_usuario)) {
					return true;
				}
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqlException) {
			throw new RuntimeException(sqlException);
		}
		return false;
	}
}
