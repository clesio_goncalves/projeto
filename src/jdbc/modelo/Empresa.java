package jdbc.modelo;

public class Empresa {
	private int codigo_empresa;
	private String cnpj_empresa;
	private String inscricao_estadual;
	private String nome_empresarial;
	private String nome_fantasia;
	private int grau_risco_empresa;
	private String cnae_empresa;
	private String atividade_principal;
	private String telefone_empresa;
	private String email_empresa;
	private String cep;
	private String nome_logradouro;
	private int numero_logradouro;
	private String bairro;
	private String uf;
	private String cidade;
	private String complemento;

	public int getCodigo_empresa() {
		return codigo_empresa;
	}

	public void setCodigo_empresa(int codigo_empresa) {
		this.codigo_empresa = codigo_empresa;
	}

	public String getCnpj_empresa() {
		return cnpj_empresa;
	}

	public void setCnpj_empresa(String cnpj_empresa) {
		this.cnpj_empresa = cnpj_empresa;
	}

	public String getInscricao_estadual() {
		return inscricao_estadual;
	}

	public void setInscricao_estadual(String inscricao_estadual) {
		this.inscricao_estadual = inscricao_estadual;
	}

	public String getNome_empresarial() {
		return nome_empresarial;
	}

	public void setNome_empresarial(String nome_empresarial) {
		this.nome_empresarial = nome_empresarial;
	}

	public String getNome_fantasia() {
		return nome_fantasia;
	}

	public void setNome_fantasia(String nome_fantasia) {
		this.nome_fantasia = nome_fantasia;
	}

	public int getGrau_risco_empresa() {
		return grau_risco_empresa;
	}

	public void setGrau_risco_empresa(int grau_risco_empresa) {
		this.grau_risco_empresa = grau_risco_empresa;
	}

	public String getCnae_empresa() {
		return cnae_empresa;
	}

	public void setCnae_empresa(String cnae_empresa) {
		this.cnae_empresa = cnae_empresa;
	}

	public String getAtividade_principal() {
		return atividade_principal;
	}

	public void setAtividade_principal(String atividade_principal) {
		this.atividade_principal = atividade_principal;
	}

	public String getTelefone_empresa() {
		return telefone_empresa;
	}

	public void setTelefone_empresa(String telefone_empresa) {
		this.telefone_empresa = telefone_empresa;
	}

	public String getEmail_empresa() {
		return email_empresa;
	}

	public void setEmail_empresa(String email_empresa) {
		this.email_empresa = email_empresa;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNome_logradouro() {
		return nome_logradouro;
	}

	public void setNome_logradouro(String nome_logradouro) {
		this.nome_logradouro = nome_logradouro;
	}

	public int getNumero_logradouro() {
		return numero_logradouro;
	}

	public void setNumero_logradouro(int numero_logradouro) {
		this.numero_logradouro = numero_logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

}
