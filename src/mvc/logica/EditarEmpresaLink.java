package mvc.logica;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.dao.EmpresaDao;
import jdbc.modelo.Empresa;

public class EditarEmpresaLink implements Logica {

	public String executa(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int codigo_empresa = Integer.parseInt(request
				.getParameter("codigo_empresa"));

		Empresa empresa = new Empresa();
		empresa.setCodigo_empresa(codigo_empresa);

		// busca a conexão pendurada na requisição
		Connection connection = (Connection) request.getAttribute("conexao");

		// Monta a lista d empresa
		List<Empresa> empresaEdit = new EmpresaDao(connection).select(empresa);

		// Guarda a lista no request
		request.setAttribute("empresa", empresaEdit);

		return "/WEB-INF/jsp/editar-empresa.jsp";
	}

}
