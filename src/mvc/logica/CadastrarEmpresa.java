package mvc.logica;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.dao.EmpresaDao;
import jdbc.modelo.Empresa;

public class CadastrarEmpresa implements Logica {
	public String executa(HttpServletRequest request,
			HttpServletResponse response) throws SQLException,
			ServletException, IOException {

		// buscando os parâmetros no request
		String cnpj_empresa = request.getParameter("cnpj_empresa");
		String inscricao_estadual = request.getParameter("inscricao_estadual");
		String nome_empresarial = request.getParameter("nome_empresarial");
		String nome_fantasia = request.getParameter("nome_empresarial");
		int grau_risco_empresa = Integer.parseInt(request
				.getParameter("grau_risco_empresa"));
		String cnae_empresa = request.getParameter("cnae_empresa");
		String atividade_principal = request
				.getParameter("atividade_principal");
		String telefone_empresa = request.getParameter("telefone_empresa");
		String email_empresa = request.getParameter("email_empresa");
		String cep = request.getParameter("cep");
		String nome_logradouro = request.getParameter("nome_logradouro");

		int numero_logradouro = 0;

		// Conversão de valores
		try {
			numero_logradouro = Integer.parseInt(request
					.getParameter("numero_logradouro"));
		} catch (NumberFormatException e) {

		}
		String bairro = request.getParameter("bairro");
		String uf = request.getParameter("uf");
		String cidade = request.getParameter("cidade");
		String complemento = request.getParameter("complemento");

		// monta um objeto empresa
		Empresa empresa = new Empresa();
		empresa.setCnpj_empresa(cnpj_empresa);
		empresa.setInscricao_estadual(inscricao_estadual);
		empresa.setNome_empresarial(nome_empresarial);
		empresa.setNome_fantasia(nome_fantasia);
		empresa.setGrau_risco_empresa(grau_risco_empresa);
		empresa.setCnae_empresa(cnae_empresa);
		empresa.setAtividade_principal(atividade_principal);
		empresa.setTelefone_empresa(telefone_empresa);
		empresa.setEmail_empresa(email_empresa);
		empresa.setCep(cep);
		empresa.setNome_logradouro(nome_logradouro);
		if (numero_logradouro > 0) {
			empresa.setNumero_logradouro(numero_logradouro);
		}
		empresa.setBairro(bairro);
		empresa.setUf(uf);
		empresa.setCidade(cidade);
		empresa.setComplemento(complemento);

		// busca a conexão pendurada na requisição
		Connection connection = (Connection) request.getAttribute("conexao");

		// salva empresa
		EmpresaDao dao = new EmpresaDao(connection);
		dao.adiciona(empresa);

		return "mvc?logica=ListarEmpresas";

	}
}
