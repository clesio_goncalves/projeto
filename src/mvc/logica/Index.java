package mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Index implements Logica {

	public String executa(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return "/WEB-INF/jsp/index.jsp";
	}

}
