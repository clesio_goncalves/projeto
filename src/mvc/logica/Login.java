package mvc.logica;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.dao.UsuarioDao;
import jdbc.modelo.Usuario;

public class Login implements Logica {

	public String executa(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		// buscando os parâmetros no request
		String email_usuario = request.getParameter("email_usuario");
		String senha_usuario = request.getParameter("senha_usuario");

		// monta um objeto usuario
		Usuario usuario = new Usuario();
		usuario.setEmail_usuario(email_usuario);
		usuario.setSenha_usuario(senha_usuario);

		// busca a conexão pendurada na requisição
		Connection connection = (Connection) request.getAttribute("conexao");

		// salva usuario
		UsuarioDao dao = new UsuarioDao(connection);

		if (dao.count(usuario) == 0) {
			dao.adiciona(usuario);
			return "mvc?logica=Index";
		} else {
			if (dao.login(usuario)) {
				return "mvc?logica=Index";
			} else {
				boolean erro = true;

				// Guarda o erro no request
				request.setAttribute("erro", erro);
				return "/WEB-INF/jsp/login.jsp";
			}
		}
	}
}
