package mvc.logica;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.dao.EmpresaDao;
import jdbc.modelo.Empresa;

public class ListarEmpresas implements Logica {

	public String executa(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		// busca a conexão pendurada na requisição
		Connection connection = (Connection) request.getAttribute("conexao");

		// Monta a lista de empresas
		List<Empresa> empresas = new EmpresaDao(connection).getLista();

		// Guarda a lista no request
		request.setAttribute("empresas", empresas);
		
		return "/WEB-INF/jsp/listar-empresas.jsp";
	}
}
