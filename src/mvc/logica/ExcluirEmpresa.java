package mvc.logica;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.dao.EmpresaDao;
import jdbc.modelo.Empresa;

public class ExcluirEmpresa implements Logica {

	public String executa(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int codigo_empresa = Integer.parseInt(request
				.getParameter("codigo_empresa"));

		Empresa empresa = new Empresa();
		empresa.setCodigo_empresa(codigo_empresa);

		// busca a conexão pendurada na requisição
		Connection connection = (Connection) request.getAttribute("conexao");

		// conexão no construtor
		EmpresaDao dao = new EmpresaDao(connection);
		dao.excluir(empresa);

		return "mvc?logica=ListarEmpresas";
	}

}
