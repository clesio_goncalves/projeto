<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Cadastrar Empresa</title>
<meta name="viewport" content="width=device-width">

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/empresa.css">

<style type="text/css">
.cor {
	color: #000000;
}
</style>
</head>
<body>

	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<c:import url="cabecalho.jsp" />

	<div class="jumbotron">
		<div class="container">
			<h1>Bem-vindo à Picos Extintor</h1>
			<p>Este é um protótipo de um sistema que, inicialmente, realiza o
				cadastro de empresas. Porém, o seu objetivo final é automatizar o
				processo de elaboração de projetos e realização de treinamentos,
				ambos na área de segurança e medicina do trabalho</p>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default col-lg-12">
				<div class="panel-heading">Objetivos inicial e final</div>


				<div class="panel-body">

					<div id="carousel-example-generic" class="carousel slide"
						data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0"
								class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<div class="item active">
								<img src="images/ProjetoInicial.png" alt="Projeto inicial"
									title="Projeto Inicial">
								<div class="carousel-caption">
									<h3 class="cor">Projeto Inicial</h3>
									<p class="cor">Projeto implementado atualmente</p>
								</div>
							</div>
							<div class="item">
								<img src="images/Projeto.png" alt="Projeto final"
									title="Projeto Final">

								<div class="carousel-caption">
									<h3 class="cor">Projeto Final</h3>
									<p class="cor">Projeto que será implementado</p>
								</div>

							</div>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic"
							role="button" data-slide="prev"> <span
							class="glyphicon glyphicon-chevron-left"></span>
						</a> <a class="right carousel-control"
							href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>

				</div>

			</div>

			<a href="mvc?logica=CadastrarEmpresaLink"
				class="btn btn-primary btn-lg"><span
				class="glyphicon glyphicon-plus"></span> Cadastrar</a>
		</div>
	</div>

	<c:import url="rodape.jsp" />

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>

</body>
</html>
