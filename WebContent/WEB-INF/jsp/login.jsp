<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Login</title>
<meta name="viewport" content="width=device-width">

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/signin.css">
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Login no Sistema</h3>
					</div>
					<div class="panel-body">
						<form role="form" action="mvc?logica=Login" method="POST">

							<div class="form-group">
								<input class="form-control" placeholder="E-mail"
									name="email_usuario" MAXLENGTH="50" type="email" required autofocus>
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Senha"
									name="senha_usuario" MAXLENGTH="50" type="password" required>
							</div>
							<button type="submit" class="btn btn-lg btn-success btn-block">Entrar</button>
						</form>

					</div>
				</div>
				<c:if test="${erro}">
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Erro!</strong> Usuário e/ou senha inválidos.
					</div>
				</c:if>
			</div>
		</div>
	</div>

	<script src="js/email.js"></script>

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
</body>

</html>
