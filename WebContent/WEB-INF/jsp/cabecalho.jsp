<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/cabecalho.css">
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-responsive-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="mvc?logica=Index"> <span
				class="glyphicon glyphicon-home"></span> Picos Extintor
			</a>
		</div>
		<div class="navbar-collapse collapse navbar-responsive-collapse">
			<ul class="nav navbar-nav">
				<li><a href="mvc?logica=ListarEmpresas">Empresas</a></li>
				<!-- <li><a href="#">Porjetos</a></li>
				<li><a href="#">EPI</a></li>
				<li><a href="#">Extintores</a></li> -->
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Cadastro <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="mvc?logica=CadastrarEmpresaLink">Empresa</a></li>
						<!-- <li><a href="#">Projeto</a></li>
						<li><a href="#">Profissional</a></li>
						<li><a href="#">Treinamento</a></li>
						<li class="divider"></li>
						<li class="dropdown-header">Extintores</li>
						<li ><a href="#">EPI</a></li> -->
					</ul></li>
			</ul>
			<!-- <form class="navbar-form navbar-left">
				<input type="text" class="form-control col-lg-8"
					placeholder="Pesquisa"> -->
			</form>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="mvc?logica=LoginLink">Sair</a></li>
				<li><a></a></li>
			</ul>
		</div>
	</nav>
</body>
</html>