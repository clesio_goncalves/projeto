<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Listar Empresas</title>
<meta name="viewport" content="width=device-width">

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/empresa.css">
<link rel="stylesheet" href="css/jquery.dataTables.css">
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<c:import url="cabecalho.jsp" />

	<div class="container">
		<div class="row">
			<div class="panel panel-default col-lg-12">
				<div class="panel-heading">Listagem de Empresas</div>

				<!-- Table -->
				<div class="panel-body">
					<div class="table-responsive">
						<table
							class="table table-striped table-hover display"
							id="table_id">
							<thead>
								<tr>
									<th>Código</th>
									<th>CNPJ</th>
									<th>Nome Empresarial</th>
									<th>Telefone</th>
									<th>E-mail</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody>
								<!-- percorre empresas montando as linhas da tabela -->
								<c:forEach var="empresa" items="${empresas}">
									<tr>
										<td>${empresa.codigo_empresa}</td>
										<td>${empresa.cnpj_empresa}</td>
										<td>${empresa.nome_empresarial}</td>
										<td><c:choose>
												<c:when test="${not empty empresa.telefone_empresa}">
										${empresa.telefone_empresa}
									</c:when>
												<c:otherwise>
										Telefone não informado
									</c:otherwise>
											</c:choose></td>

										<td><c:choose>
												<c:when test="${not empty empresa.email_empresa}">
													<a href="mailto:${empresa.email_empresa}">${empresa.email_empresa}</a>
												</c:when>
												<c:otherwise>
											E-mail não informado
										</c:otherwise>
											</c:choose></td>

										<td><a
											href="mvc?logica=ExibirEmpresaLink&codigo_empresa=${empresa.codigo_empresa}"
											class="btn btn-success btn-xs"><span
												class="glyphicon glyphicon-zoom-in"></span> Exibir</a> <a
											href="mvc?logica=EditarEmpresaLink&codigo_empresa=${empresa.codigo_empresa}"
											class="btn btn-info btn-xs"><span
												class="glyphicon glyphicon-edit"></span> Editar </a> <!-- Botão exluir -->
											<button class="btn btn-danger btn-xs" data-toggle="modal"
												data-target="#${empresa.codigo_empresa}">
												<span class="glyphicon glyphicon-trash"></span> Excluir
											</button> <!-- Modal -->
											<div class="modal fade" id="${empresa.codigo_empresa}"
												tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
												aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">
																<span aria-hidden="true">&times;</span><span
																	class="sr-only">Fechar</span>
															</button>
															<h4 class="modal-title" id="myModalLabel">Exclusão
																de empresa</h4>
														</div>
														<div class="modal-body">Deseja realmente excluir a
															empresa (${empresa.codigo_empresa}) ->
															${empresa.nome_empresarial}?</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default"
																data-dismiss="modal">
																<span class="glyphicon glyphicon-log-out"></span> Fechar
															</button>
															<a
																href="mvc?logica=ExcluirEmpresa&codigo_empresa=${empresa.codigo_empresa}"
																class="btn btn-danger"><span
																class="glyphicon glyphicon-trash"></span> Excluir</a>
														</div>
													</div>
												</div>
											</div></td>
									</tr>
								</c:forEach>
							</tbody>

						</table>
					</div>
				</div>
			</div>
			<a href="mvc?logica=CadastrarEmpresaLink"
				class="btn btn-primary btn-lg"><span
				class="glyphicon glyphicon-plus"></span> Cadastrar</a>
		</div>
	</div>


	<c:import url="rodape.jsp" />

	<!-- jQuery -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>

	<!-- DataTables -->
	<script src="js/jquery.dataTables.js"></script>
	
	<script>
		$(document).ready(function() {
			$('#table_id').DataTable({
				"language" : {
					"url" : "idioma/Portuguese-Brasil.json"
				}
			});
		});
	</script>
	
</body>
</html>