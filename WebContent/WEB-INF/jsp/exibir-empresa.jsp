<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Exibir Dados da Empresa</title>
<meta name="viewport" content="width=device-width">

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/empresa.css">
</head>
<body>

	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<c:import url="cabecalho.jsp" />

	<div class="container">
		<div class="row">
			<!-- percorre empresas montando as linhas da tabela -->
			<c:forEach var="empresa" items="${empresa}">
				<div class="panel panel-default col-sm-12">
					<div class="panel-heading">Exibir Dados da Empresa</div>
					<!-- Table -->
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-hover">

								<tr>
									<th>Código</th>
									<td>${empresa.codigo_empresa}</td>
								</tr>

								<tr>
									<th>CNPJ</th>
									<td>${empresa.cnpj_empresa}</td>
								</tr>

								<tr>
									<th>Inscrição Estadual</th>
									<td>${empresa.inscricao_estadual}</td>
								</tr>

								<tr>
									<th>Nome Empresarial</th>
									<td>${empresa.nome_empresarial}</td>
								</tr>

								<tr>
									<th>Nome Fantasia</th>
									<td><c:choose>
											<c:when test="${not empty empresa.nome_fantasia}">
									${empresa.nome_fantasia}
								</c:when>
											<c:otherwise>
											Nome fantasia não informado
								</c:otherwise>
										</c:choose></td>
								</tr>

								<tr>
									<th>Grau de Risco</th>
									<td>${empresa.grau_risco_empresa}</td>
								</tr>

								<tr>
									<th>CNAE</th>
									<td>${empresa.cnae_empresa}</td>
								</tr>

								<tr>
									<th>Atividade Principal</th>
									<td>${empresa.atividade_principal}</td>
								</tr>



								<tr>
									<th>Telefone</th>
									<td><c:choose>
											<c:when test="${not empty empresa.telefone_empresa}">
									${empresa.telefone_empresa}
								</c:when>
											<c:otherwise>
											Telefone não informado
								</c:otherwise>
										</c:choose></td>
								</tr>

								<tr>
									<th>E-mail</th>
									<td><c:choose>
											<c:when test="${not empty empresa.email_empresa}">
												<a href="mailto:${empresa.email_empresa}">${empresa.email_empresa}</a>
											</c:when>
											<c:otherwise>
											E-mail não informado
										</c:otherwise>
										</c:choose></td>
								</tr>

								<tr>
									<th>CEP</th>
									<td>${empresa.cep}</td>
								</tr>

								<tr>
									<th>Nome Logradouro</th>
									<td>${empresa.nome_logradouro}</td>
								</tr>

								<tr>
									<th>Número Logradouro</th>
									<td><c:choose>
											<c:when test="${empresa.numero_logradouro > 0}">
									${empresa.numero_logradouro}
								</c:when>
											<c:otherwise>
											Sem Número
								</c:otherwise>
										</c:choose></td>
								</tr>

								<tr>
									<th>Bairro</th>
									<td>${empresa.bairro}</td>
								</tr>

								<tr>
									<th>UF</th>
									<td>${empresa.uf}</td>
								</tr>

								<tr>
									<th>Cidade</th>
									<td>${empresa.cidade}</td>
								</tr>

								<tr>
									<th>Complemento</th>
									<td><c:choose>
											<c:when test="${not empty empresa.complemento}">
									${empresa.complemento}
								</c:when>
											<c:otherwise>
											Complemento não informado
								</c:otherwise>
										</c:choose></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div align="center">
					<a href="mvc?logica=CadastrarEmpresaLink"
						class="btn btn-primary btn-lg"><span
						class="glyphicon glyphicon-plus"></span> Cadastrar</a> <a
						href="mvc?logica=EditarEmpresaLink&codigo_empresa=${empresa.codigo_empresa}"
						class="btn btn-info btn-lg"><span
						class="glyphicon glyphicon-edit"></span> Editar </a>

					<button class="btn btn-danger btn-lg" data-toggle="modal"
						data-target="#${empresa.codigo_empresa}">
						<span class="glyphicon glyphicon-trash"></span> Excluir
					</button>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="${empresa.codigo_empresa}" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">Exclusão de
									empresa</h4>
							</div>
							<div class="modal-body">Deseja realmente excluir a empresa
								(${empresa.codigo_empresa}) -> ${empresa.nome_empresarial}?</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">
									<span class="glyphicon glyphicon-log-out"></span> Fechar
								</button>
								<a
									href="mvc?logica=ExcluirEmpresa&codigo_empresa=${empresa.codigo_empresa}"
									class="btn btn-danger"><span
									class="glyphicon glyphicon-trash"></span> Excluir</a>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
			<ul class="pager">
				<li class="previous"><a href="mvc?logica=ListarEmpresas"><span
						class="glyphicon glyphicon-chevron-left"></span> Voltar</a></li>
			</ul>
		</div>
	</div>

	<c:import url="rodape.jsp" />

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>

</body>
</html>
