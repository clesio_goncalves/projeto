<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Cadastrar Empresa</title>
<meta name="viewport" content="width=device-width">

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/empresa.css">
</head>
<body>

	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<c:import url="cabecalho.jsp" />

	<div class="jumbotron">
		<div class="container">
			<h1>Cadastrar Empresa</h1>
			<p>Preencha o formulário abaixo para realizar o cadastro.</p>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<form action="mvc?logica=CadastrarEmpresa" method="POST"
				class="col-sm-12">
				<div class="row">

					<fieldset class="col-md-6">
						<legend>Dados empresariais</legend>

						<div class="row">
							<!-- CNPJ -->
							<div class="form-group col-sm-6">
								<label for="cnpj">CNPJ*</label> <input type="text"
									class="form-control" name="cnpj_empresa" MAXLENGTH="18"
									autofocus placeholder="00.000.000/0000-00"
									data-mask="99.999.999/9999-99" required>
							</div>

							<!-- INSCRIÇÃO ESTADUAL -->
							<div class="form-group col-sm-6">
								<label for="inscricao_estadual">Inscrição Estatudal</label> <input
									type="text" class="form-control" name="inscricao_estadual"
									placeholder="00.000.000-0" data-mask="99.999.999-9"
									maxlength="12">
							</div>
						</div>

						<!-- NOME EMPRESARIAL -->
						<div class="form-group">
							<label for="nome_empresarial">Nome Empresarial*</label> <input
								type="text" class="form-control" name="nome_empresarial"
								MAXLENGTH="100" required>
						</div>

						<!-- NOME FANTASIA -->
						<div class="form-group">
							<label for="nome_fantasia">Nome Fantasia</label> <input
								type="text" class="form-control" name="nome_fantasia"
								MAXLENGTH="100">
						</div>

						<div class="row">
							<!-- GRUAU DE RISCO -->
							<div class="form-group col-sm-3">
								<label for="grau_risco">Grau de Risco*</label> <select
									class="form-control" name="grau_risco_empresa" required>
									<!-- Posso utilizar o datalist -->
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
								</select>
							</div>

							<!-- CNAE -->
							<div class="form-group col-sm-9">
								<label for="cnae">CNAE*</label> <input type="text"
									class="form-control" name="cnae_empresa" MAXLENGTH="10"
									placeholder="00.00-0-00" data-mask="99.99-9-99" required>
							</div>
						</div>

						<!-- ATIVIDADE PRINCIPAL -->
						<div class="form-group">
							<label for="atividade_principal">Atividade Principal*</label>
							<textarea class="form-control" name="atividade_principal"
								required></textarea>
						</div>

						<div class="row">
							<!-- TELEFONE -->
							<div class="form-group col-sm-4">
								<label for="telefone">Telefone</label> <input type="text"
									class="form-control" name="telefone_empresa" MAXLENGTH="13"
									placeholder="(00)0000-0000" data-mask="(99)9999-9999">
							</div>

							<!-- EMAIL -->
							<div class="form-group col-sm-8">
								<label for="email">Email</label>
								<div class="input-group">
									<span class="input-group-addon">@</span> <input type="email"
										class="form-control" name="email_empresa" MAXLENGTH="50"
										placeholder="email@exemplo.com">
								</div>
							</div>
						</div>
					</fieldset>

					<fieldset class="col-md-6">
						<legend>Endereço</legend>


						<!-- CEP -->
						<div class="form-group">
							<label for="cep">CEP*</label> <input type="text"
								class="form-control" name="cep" MAXLENGTH="10"
								placeholder="00.000-000" data-mask="99.999-999" required>
						</div>

						<div class="row">
							<!-- NOME LOGRADOURO -->
							<div class="form-group col-sm-9">
								<label for="nome_logradouro">Nome logradouro*</label> <input
									type="text" class="form-control" name="nome_logradouro"
									MAXLENGTH="50" required>
							</div>

							<!-- NÚMERO LOGRADOURO -->
							<div class="form-group col-sm-3">
								<label for="num_logradouro">Número</label> <input type="text"
									class="form-control" name="numero_logradouro" MAXLENGTH="11"
									onkeypress='return SomenteNumero(event)'>
							</div>
						</div>

						<!-- BAIRRO -->
						<div class="form-group">
							<label for="bairro">Bairro*</label> <input type="text"
								class="form-control" name="bairro" MAXLENGTH="50" required>
						</div>

						<div class="row">
							<!-- ESTADO-->
							<div class="form-group col-sm-3">
								<label for="estado">Estado*</label> <select class="form-control"
									name="uf" required>
									<!-- Posso utilizar o datalist -->
									<option value="AC">AC</option>
									<option value="AL">AL</option>
									<option value="AM">AM</option>
									<option value="AP">AP</option>
									<option value="BA">BA</option>
									<option value="CE">CE</option>
									<option value="DF">DF</option>
									<option value="ES">ES</option>
									<option value="GO">GO</option>
									<option value="MA">MA</option>
									<option value="MG">MG</option>
									<option value="MS">MS</option>
									<option value="MT">MT</option>
									<option value="PA">PA</option>
									<option value="PB">PB</option>
									<option value="PE">PE</option>
									<option value="PI">PI</option>
									<option value="PR">PR</option>
									<option value="RJ">RJ</option>
									<option value="RN">RN</option>
									<option value="RO">RO</option>
									<option value="RR">RR</option>
									<option value="RS">RS</option>
									<option value="SC">SC</option>
									<option value="SE">SE</option>
									<option value="SP">SP</option>
									<option value="TO">TO</option>
								</select>
							</div>

							<!-- CIDADE -->
							<div class="form-group col-sm-9">
								<label for="cidade">Cidade*</label> <input type="text"
									class="form-control" name="cidade" MAXLENGTH="50" required>
							</div>
						</div>

						<!-- COMPLEMENTO -->
						<div class="form-group">
							<label for="complemento">Complemento</label> <input type="text"
								class="form-control" name="complemento" MAXLENGTH="50">
						</div>

						<!-- OBTIGATÓRIO -->
						<label for="obrigatorio">(*) Campos obrigatórios</label>
						<button type="submit" class="btn btn-primary btn-lg pull-right">
							<span class="glyphicon glyphicon-thumbs-up"></span> Salvar
						</button>
					</fieldset>
				</div>
			</form>
			<ul class="pager">
				<li class="previous"><a href="mvc?logica=Index"><span
						class="glyphicon glyphicon-chevron-left"></span> Picos Extintor</a></li>
			</ul>
		</div>
	</div>

	<c:import url="rodape.jsp" />

	<script src="js/SomenteNumero.js"></script>
	<script src="js/email.js"></script>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/inputmask-plugin.js"></script>

</body>
</html>
