SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `Projeto`.`Profissional`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Profissional` (
  `codigo_prof` INT NOT NULL,
  `nome_prof` TEXT NOT NULL,
  `telefone_prof` VARCHAR(13) NULL,
  `email_prof` TEXT NULL,
  PRIMARY KEY (`codigo_prof`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Empresa` (
  `codigo_empresa` INT NULL AUTO_INCREMENT,
  `cnpj_empresa` VARCHAR(18) NOT NULL,
  `inscricao_estadual` VARCHAR(12) NULL DEFAULT 'ISENTO',
  `nome_empresarial` TEXT NOT NULL,
  `nome_fantasia` TEXT NULL,
  `grau_risco_empresa` ENUM('1', '2', '3', '4') NOT NULL,
  `cnae_empresa` VARCHAR(10) NOT NULL,
  `atividade_principal` TEXT NOT NULL,
  `telefone_empresa` VARCHAR(13) NULL,
  `email_empresa` TEXT NULL,
  `cep` VARCHAR(10) NOT NULL,
  `nome_logradouro` TEXT NOT NULL,
  `numero_logradouro` INT NULL DEFAULT NULL,
  `bairro` TEXT NOT NULL,
  `uf` VARCHAR(2) NOT NULL,
  `cidade` TEXT NOT NULL,
  `complemento` TEXT NULL,
  PRIMARY KEY (`codigo_empresa`),
  UNIQUE INDEX `CNPJ_empresa_UNIQUE` (`cnpj_empresa` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Setor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Setor` (
  `codigo_setor` INT NULL AUTO_INCREMENT,
  `nome_setor` TEXT NOT NULL,
  `codigo_empresa` INT NOT NULL,
  PRIMARY KEY (`codigo_setor`),
  INDEX `fk_tbSetor_tbEmpresa1_idx` (`codigo_empresa` ASC),
  UNIQUE INDEX `nome_setor_UNIQUE` (`nome_setor` ASC),
  CONSTRAINT `fk_tbSetor_tbEmpresa1`
    FOREIGN KEY (`codigo_empresa`)
    REFERENCES `Projeto`.`Empresa` (`codigo_empresa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Cargo` (
  `codigo_cargo` INT NULL AUTO_INCREMENT,
  `nome_cargo` TEXT NOT NULL,
  `descricao_cargo` TEXT NOT NULL,
  `qnt_horas_cargo` INT NOT NULL,
  PRIMARY KEY (`codigo_cargo`),
  UNIQUE INDEX `nome_cargo_UNIQUE` (`nome_cargo` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Empregado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Empregado` (
  `codigo_empregado` INT NOT NULL,
  `nome_empregado` TEXT NOT NULL,
  `sexo_empregado` ENUM('M', 'F') NOT NULL,
  `codigo_setor` INT NOT NULL,
  `codigo_cargo` INT NOT NULL,
  PRIMARY KEY (`codigo_empregado`),
  INDEX `fk_Empregado_Cargo1_idx` (`codigo_cargo` ASC),
  INDEX `fk_tbEmpregado_tbSetor1_idx` (`codigo_setor` ASC),
  CONSTRAINT `fk_tbEmpregado_tbSetor1`
    FOREIGN KEY (`codigo_setor`)
    REFERENCES `Projeto`.`Setor` (`codigo_setor`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Empregado_Cargo1`
    FOREIGN KEY (`codigo_cargo`)
    REFERENCES `Projeto`.`Cargo` (`codigo_cargo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Projeto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Projeto` (
  `codigo_projeto` INT NULL AUTO_INCREMENT,
  `nome_projeto` TEXT NOT NULL,
  `descricao_projeto` TEXT NOT NULL,
  `conclusao_projeto` TEXT NOT NULL,
  `valor_projeto` DECIMAL(10,2) NOT NULL,
  `data_validade` DATE NOT NULL,
  PRIMARY KEY (`codigo_projeto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Elaboracao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Elaboracao` (
  `codigo_projeto` INT NOT NULL,
  `codigo_empresa` INT NOT NULL,
  `codigo_prof` INT NOT NULL,
  `data` DATE NOT NULL,
  PRIMARY KEY (`codigo_projeto`, `codigo_empresa`, `codigo_prof`, `data`),
  INDEX `fk_tbProfissional_has_tbEmpresa_tbProjeto1_idx` (`codigo_projeto` ASC),
  INDEX `fk_Elaboracao_Profissional1_idx` (`codigo_prof` ASC),
  INDEX `fk_tbProfissional_has_tbEmpresa_tbEmpresa1_idx` (`codigo_empresa` ASC),
  CONSTRAINT `fk_tbProfissional_has_tbEmpresa_tbEmpresa1`
    FOREIGN KEY (`codigo_empresa`)
    REFERENCES `Projeto`.`Empresa` (`codigo_empresa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbProfissional_has_tbEmpresa_tbProjeto1`
    FOREIGN KEY (`codigo_projeto`)
    REFERENCES `Projeto`.`Projeto` (`codigo_projeto`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Elaboracao_Profissional1`
    FOREIGN KEY (`codigo_prof`)
    REFERENCES `Projeto`.`Profissional` (`codigo_prof`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Contato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Contato` (
  `codigo_empresa` INT NOT NULL,
  `nome_contato` VARCHAR(50) NOT NULL,
  `telefone_contato` VARCHAR(13) NOT NULL,
  INDEX `fk_tbContato_tbEmpresa1_idx` (`codigo_empresa` ASC),
  PRIMARY KEY (`nome_contato`, `codigo_empresa`),
  CONSTRAINT `fk_tbContato_tbEmpresa1`
    FOREIGN KEY (`codigo_empresa`)
    REFERENCES `Projeto`.`Empresa` (`codigo_empresa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Treinamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Treinamento` (
  `codigo_prof` INT NOT NULL,
  `codigo_empregado` INT NOT NULL,
  `codigo_empresa` INT NOT NULL,
  `data_inicio` DATE NOT NULL,
  `data_fim` DATE NOT NULL,
  `conteudo_programatico` TEXT NOT NULL,
  `carga_horaria` INT NOT NULL,
  `valor` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`codigo_prof`, `codigo_empregado`, `codigo_empresa`, `data_inicio`, `data_fim`),
  INDEX `fk_tbProfissionalEmpregadoEmpresa_tbEmpregado1_idx` (`codigo_empregado` ASC),
  INDEX `fk_tbProfissionalEmpregadoEmpresa_tbProfissional1_idx` (`codigo_prof` ASC),
  INDEX `fk_tbProfissionalEmpregadoEmpresa_tbEmpresa1_idx` (`codigo_empresa` ASC),
  CONSTRAINT `fk_tbProfissionalEmpregadoEmpresa_tbEmpregado1`
    FOREIGN KEY (`codigo_empregado`)
    REFERENCES `Projeto`.`Empregado` (`codigo_empregado`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbProfissionalEmpregadoEmpresa_tbProfissional1`
    FOREIGN KEY (`codigo_prof`)
    REFERENCES `Projeto`.`Profissional` (`codigo_prof`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbProfissionalEmpregadoEmpresa_tbEmpresa1`
    FOREIGN KEY (`codigo_empresa`)
    REFERENCES `Projeto`.`Empresa` (`codigo_empresa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Epi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Epi` (
  `codigo_epi` INT NULL AUTO_INCREMENT,
  `nome_epi` TEXT NOT NULL,
  `descricao_epi` TEXT NOT NULL,
  `codigo_empresa` INT NOT NULL,
  PRIMARY KEY (`codigo_epi`),
  UNIQUE INDEX `nome_EPI_UNIQUE` (`nome_epi` ASC),
  INDEX `fk_Epi_Empresa1_idx` (`codigo_empresa` ASC),
  CONSTRAINT `fk_Epi_Empresa1`
    FOREIGN KEY (`codigo_empresa`)
    REFERENCES `Projeto`.`Empresa` (`codigo_empresa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Usuario` (
  `codigo_usuario` INT NOT NULL AUTO_INCREMENT,
  `nome_usuario` TEXT NOT NULL,
  `senha_usuario` VARCHAR(50) NOT NULL,
  `codigo_prof` INT NOT NULL,
  PRIMARY KEY (`codigo_usuario`),
  UNIQUE INDEX `nome_usuario_UNIQUE` (`nome_usuario` ASC),
  INDEX `fk_Usuario_Profissional1_idx` (`codigo_prof` ASC),
  CONSTRAINT `fk_Usuario_Profissional1`
    FOREIGN KEY (`codigo_prof`)
    REFERENCES `Projeto`.`Profissional` (`codigo_prof`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`CargoEpi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`CargoEpi` (
  `codigo_cargo` INT NOT NULL,
  `codigo_epi` INT NOT NULL,
  PRIMARY KEY (`codigo_cargo`, `codigo_epi`),
  INDEX `fk_Cargo_has_Epi_Epi1_idx` (`codigo_epi` ASC),
  INDEX `fk_Cargo_has_Epi_Cargo1_idx` (`codigo_cargo` ASC),
  CONSTRAINT `fk_Cargo_has_Epi_Cargo1`
    FOREIGN KEY (`codigo_cargo`)
    REFERENCES `Projeto`.`Cargo` (`codigo_cargo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Cargo_has_Epi_Epi1`
    FOREIGN KEY (`codigo_epi`)
    REFERENCES `Projeto`.`Epi` (`codigo_epi`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Area`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Area` (
  `codigo_area` INT NOT NULL AUTO_INCREMENT,
  `nome_area` TEXT NOT NULL,
  PRIMARY KEY (`codigo_area`),
  UNIQUE INDEX `nome_area_UNIQUE` (`nome_area` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`Especialidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`Especialidade` (
  `codigo_especialidade` INT NOT NULL AUTO_INCREMENT,
  `nome_especialidade` TEXT NOT NULL,
  `descricao_especialidade` TEXT NOT NULL,
  `codigo_area` INT NOT NULL,
  PRIMARY KEY (`codigo_especialidade`),
  INDEX `fk_Especialidade_Area1_idx` (`codigo_area` ASC),
  CONSTRAINT `fk_Especialidade_Area1`
    FOREIGN KEY (`codigo_area`)
    REFERENCES `Projeto`.`Area` (`codigo_area`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Projeto`.`EspecialidadeProfissional`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Projeto`.`EspecialidadeProfissional` (
  `codigo_especialidade` INT NOT NULL,
  `codigo_prof` INT NOT NULL,
  `registro` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`codigo_especialidade`, `codigo_prof`),
  INDEX `fk_Especialidade_has_Profissional_Profissional1_idx` (`codigo_prof` ASC),
  INDEX `fk_Especialidade_has_Profissional_Especialidade1_idx` (`codigo_especialidade` ASC),
  CONSTRAINT `fk_Especialidade_has_Profissional_Especialidade1`
    FOREIGN KEY (`codigo_especialidade`)
    REFERENCES `Projeto`.`Especialidade` (`codigo_especialidade`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Especialidade_has_Profissional_Profissional1`
    FOREIGN KEY (`codigo_prof`)
    REFERENCES `Projeto`.`Profissional` (`codigo_prof`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
